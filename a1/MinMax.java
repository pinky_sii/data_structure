/**
 * Created by Siripatsornsirichai on 4/26/2017 AD.
 */
public class MinMax {
    public static double minMaxAverage(int[] numbers) {
        // your code goes here
        int myMin;
        int myMax;
        int n = numbers.length;
        if (numbers.length%2==0) {
            if (numbers[0] > numbers[1]) {
                myMax = numbers[0];
                myMin = numbers[1];
            } else {
                myMax = numbers[1];
                myMin = numbers[0];
            }
        } else {
                myMax = numbers[n-1];
                myMin = numbers[n-1];
                n = n - 1;
            }

        for (int i = 1; i < n ; i += 2) {
            if (numbers[i] > numbers[i - 1]) {
                if (numbers[i] > myMax)
                    myMax = numbers[i];
                if (numbers[i-1] < myMin)
                    myMin = numbers[i-1];
            } else {
                if (numbers[i-1] > myMax)
                    myMax = numbers[i-1];
                if (numbers[i] < myMin)
                    myMin = numbers[i];
            }
        }
        return (myMin + myMax) / 2.0;
    }
}
