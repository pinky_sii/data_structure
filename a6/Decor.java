import java.sql.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Siripatsornsirichai on 6/23/2017 AD.
 */
public class Decor {

    public static BinaryTreeNode mkTree(List<Integer> postOrder, List<Integer> inOrder) {
        if(postOrder.isEmpty() && inOrder.isEmpty()) {
            return null;
        }
        else {
            Integer root = postOrder.get(postOrder.size()-1);
            postOrder.remove(root);
            int index =0;
            for (int i=0; i < inOrder.size(); i++) {
                if(inOrder.get(i) == root) {
                    index = i;
                    break;
                }
            }
            List<Integer> inLeft = new ArrayList<>();
            List<Integer> inRight = new ArrayList<>();  //inOrder.subList(index+1, inOrder.size());
            for (int j = 0; j < inOrder.size(); j++) {
                if(j < index) {
                    inLeft.add(inOrder.get(j));
                } else if(j==index) continue;
                else inRight.add(inOrder.get(j));
            }

            List<Integer> postLeft = new ArrayList<>(); //postOrder.subList(0,index);
            List<Integer> postRight = new ArrayList<>(); //postOrder.subList(index, postOrder.size());
            for (int k = 0; k < postOrder.size(); k++) {
                if(k < index) postLeft.add(postOrder.get(k));
                else postRight.add(postOrder.get(k));
            }

            BinaryTreeNode right = mkTree(postRight,inRight);
            BinaryTreeNode left = mkTree(postLeft,inLeft);
            return new BinaryTreeNode(left,root,right);
        }
    }


}
