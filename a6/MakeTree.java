import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Siripatsornsirichai on 6/22/2017 AD.
 */
public class MakeTree {

    public static BinaryTreeNode BinaryHelper(int[] keys, int lo, int hi) {
        if (hi >= lo) {
            int mid=lo+(hi-lo)/2;
            BinaryTreeNode left = BinaryHelper(keys,lo,mid-1);
            BinaryTreeNode right = BinaryHelper(keys,mid+1,hi);
            return new BinaryTreeNode(left,keys[mid],right);
        }
        return null;
    }

    public static BinaryTreeNode buildBST(int[] keys) {
        Arrays.sort(keys);
        return BinaryHelper(keys, 0, keys.length-1);
    }
}
