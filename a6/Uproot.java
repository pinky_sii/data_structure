import java.util.*;

/**
 * Created by Siripatsornsirichai on 6/22/2017 AD.
 */
public class Uproot {

    public static HashMap<Integer, Integer> treeToParentMap(BinaryTreeNode T) {
        HashMap<Integer, Integer> map = new HashMap<>();
        LinkedList<BinaryTreeNode> tree = new LinkedList<>();
        tree.add(T);
        while(!tree.isEmpty()) {
            BinaryTreeNode node = tree.poll();
            if(node.left != null) {
                map.put(node.left.key,node.key);
                tree.add(node.left);
            }
            if(node.right != null) {
                map.put(node.right.key,node.key);
                tree.add(node.right);
            }
        }
        return map;

    }
    public static BinaryTreeNode parentMapToTree(Map<Integer, Integer> map) {
        LinkedList<BinaryTreeNode> tree = new LinkedList<>();
        BinaryTreeNode treeNode = new BinaryTreeNode(0);
        //find root
        for (Integer entry : map.values()) {
            Integer parent = entry;
            if(!map.keySet().contains(parent)) {
                treeNode = new BinaryTreeNode(parent);
                tree.add(treeNode);
                break;
            }
        }
        while (!tree.isEmpty()) {
            BinaryTreeNode root = tree.poll();
            for (Map.Entry<Integer,Integer> entry : map.entrySet()) {
                Integer node = entry.getKey();
                Integer parent = entry.getValue();
                if (parent == root.key) {
                    if (root.left == null) {
                        root.left = new BinaryTreeNode(node);
                        tree.add(root.left);
                    } else {
                        root.right = new BinaryTreeNode(node);
                        tree.add(root.right);
                    }
                }
            }
        }
        return treeNode;
    }

    public static void main(String[] args) {
        BinaryTreeNode leftest = new BinaryTreeNode(null,14,null);
        BinaryTreeNode left =new BinaryTreeNode(leftest,20,null);
        BinaryTreeNode rightL = new BinaryTreeNode(2);
        BinaryTreeNode rightR = new BinaryTreeNode(18);
        BinaryTreeNode right = new BinaryTreeNode(rightL,9,rightR);
        BinaryTreeNode main = new BinaryTreeNode(left,1,right);
      //  treeToParentMap(main);
        HashMap<Integer,Integer> map = treeToParentMap(main);
        BinaryTreeNode parent = parentMapToTree(map);
        treeToParentMap(parent);

    }

}
