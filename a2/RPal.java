/**
 * Created by Siripatsornsirichai on 5/10/2017 AD.
 */
public class RPal {
    public static int countRPal(int N) {
        int count = 1;
        if (N<=1) return 1;
        else {
            for(int i = 1; i <= N/2; i++) {
                count += countRPal(i);
            }
            return count;
        }
    }
}
