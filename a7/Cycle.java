import java.util.*;

/**
 * Created by Siripatsornsirichai on 7/8/2017 AD.
 */
public class Cycle {

    private Map<String, Set<String>> adjTable;

    public Cycle(ArrayList<Pair<String,String>> graph) {
        adjTable = new HashMap<>();
        for (Pair<String,String> pair : graph ) {
            String u = pair.first;
            String v = pair.second;
            if(!adjTable.containsKey(u)) {
                adjTable.put(u,new HashSet<>());
            }
            if(!adjTable.containsKey(v)) {
                adjTable.put(v, new HashSet<>());
            }
            adjTable.get(u).add(v);
        }
    }


    public static void DFS(Map<String,Set<String>> table, Set<String> visited, Set<String> visit_set, Stack<String> visit_stack, Set<String> cycle,  String str) {
        if(!visit_set.contains(str)) {
            visited.add(str);
            for (String s: table.get(str)) {
                visit_set.add(str);
                visit_stack.add(str);
                if (visit_set.contains(s)) {
                    while (!visit_stack.peek().equals(s)) {
                        cycle.add(visit_stack.pop());
                    }
                    cycle.add(visit_stack.pop());
                }
                DFS(table,visited,visit_set,visit_stack,cycle,s);
            }
        }
        visit_set.clear();
        visit_stack.clear();
    }


    public static List<String> findCycle(ArrayList<Pair<String, String>> graph) {

        Cycle directed = new Cycle(graph);
       // System.out.println(directed.adjTable);
        Set<String> visit_set = new HashSet<>();
        Stack<String> visit_stack = new Stack<>();
        Set<String> cycle = new HashSet<>();
        Set<String> visited = new HashSet<>();
        for (String str: directed.adjTable.keySet()) {
            if(!visited.contains(str)) {
                DFS(directed.adjTable, visited, visit_set, visit_stack, cycle, str);
            }
        }
        if (cycle.isEmpty()) return null;
        else return new ArrayList<>(cycle);
    }

   /* public static void main(String[] args) {
        ArrayList<Pair<String,String>> t1 = new ArrayList<>();
        t1.add(new Pair<>("A","B"));
        t1.add(new Pair<>("A","E"));
        t1.add(new Pair<>("A","D"));
        t1.add(new Pair<>("B","C"));
        t1.add(new Pair<>("B","D"));
        t1.add(new Pair<>("C","A"));
        t1.add(new Pair<>("C","D"));
        t1.add(new Pair<>("E","D"));
        t1.add(new Pair<>("F","C"));
        t1.add(new Pair<>("F","G"));
        t1.add(new Pair<>("G","F"));
        System.out.println(findCycle(t1));

        ArrayList<Pair<String,String>> t2 = new ArrayList<>();
        t2.add(new Pair<>("1","2"));
        t2.add(new Pair<>("1","6"));
        t2.add(new Pair<>("2","4"));
        t2.add(new Pair<>("2","6"));
        t2.add(new Pair<>("3","7"));
        t2.add(new Pair<>("4","3"));
        t2.add(new Pair<>("4","8"));
        t2.add(new Pair<>("6","7"));
        t2.add(new Pair<>("7","3"));
        System.out.println(findCycle(t2));

    } */ 
}
