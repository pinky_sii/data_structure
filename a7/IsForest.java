import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Siripatsornsirichai on 7/8/2017 AD.
 */
public class IsForest {

    public static class Union {
        private int[] id;
        private int[] sz;

        public Union(int n) {
            id = new int[n];
            sz = new int[n];
            for (int i = 0; i < n ; i++) {
                id[i] = i;
                sz[i] = 1;
            }
        }

        public int root(int[] id, int p) {
            if (id[p]==p) return p;
            else {
                id[p] = root(id,id[p]);
                return id[p];
            }
        }

        public void union(int u, int v) {
            int ru = root(id, u);
            int rv = root(id, v);
            if (sz[ru] > sz[rv]) {
                id[rv] = ru; //change parent of rv to ru
                sz[ru] += sz[rv];
            } else {
                id[ru] = rv;     // ซ้ายต่อขวา
                sz[rv] += sz[ru];
            }
        }

    }

    public static int identifyTrees(int n, Iterable<Pair<Integer, Integer>> edges) {
        Union un = new Union(n);
        for (Pair<Integer,Integer> edge : edges) {
            int u = un.root(un.id,edge.first);
            int v = un.root(un.id,edge.second);
            //check if it form cycle or not, if not -> union
            if(u!=v) {
                un.union(u,v);
            } else {   // found cycle
                return 0;
            }
        }
        //System.out.println(Arrays.toString(un.id));
        int tree = 0;
        for (int i = 0; i < un.id.length; i++) {
            if(un.id[i] == i) {
                tree++;
            }
        }
        return tree;
    }

   /* public static void main(String[] args) {
        List<Pair<Integer,Integer>> edges = new ArrayList<>();
        Pair<Integer,Integer> e1 = new Pair(2,8);
        edges.add(e1);
        Pair<Integer,Integer> e2 = new Pair(1,3);
        edges.add(e2);
        Pair<Integer,Integer> e3 = new Pair(3,0);
        edges.add(e3);
        Pair<Integer,Integer> e4 = new Pair(5,2);
        edges.add(e4);
        Pair<Integer,Integer> e5 = new Pair(8,6);
        edges.add(e5);
        Pair<Integer,Integer> e6 = new Pair(2,4);
        edges.add(e6);

        List<Pair<Integer,Integer>> edges2 = new ArrayList<>();
        Pair<Integer,Integer> d1 = new Pair(1,2);
        edges2.add(d1);
        Pair<Integer,Integer> d2 = new Pair(0,3);
        edges2.add(d2);
        Pair<Integer,Integer> d3 = new Pair(4,2);
        edges2.add(d3);
        Pair<Integer,Integer> d4 = new Pair(4,5);
        edges2.add(d4);
        Pair<Integer,Integer> d5 = new Pair(1,6);
        edges2.add(d5);
        Pair<Integer,Integer> d6 = new Pair(6,5);
        edges2.add(d6);
        int result = identifyTrees(7,edges2);
        System.out.println(result);
    } */
}
