import java.util.Comparator;
import java.util.LinkedList;
import java.util.PriorityQueue;
import static com.sun.xml.internal.xsom.impl.UName.comparator;

public class MultiwayMerge {
  public static LinkedList<Integer> mergeAll(LinkedList<Integer>[] lists) {
      int n = lists.length;
      //PriorityQueue<LinkedList<Integer>> queue = new PriorityQueue<>(n,(LinkedList<Integer> a,LinkedList<Integer> b)-> Integer.compare(a.peek(),b.peek()));
      PriorityQueue<LinkedList<Integer>> queue = new PriorityQueue<>(new Comparator<LinkedList<Integer>>() {
          @Override
          public int compare(LinkedList<Integer> o1, LinkedList<Integer> o2) {
              return Integer.compare(o1.getFirst(),o2.getFirst());
          }
      });
      for (LinkedList<Integer> elt: lists) {
              queue.add(elt);
      }

      LinkedList<Integer> result = new LinkedList<>();

      while (queue.size() > 0) {
          LinkedList<Integer> headList = queue.poll();
          Integer head = headList.poll();
          if(!headList.isEmpty()) {
              queue.add(headList);
          }
          result.add(head);
      }
      return result;
  }

  public static void main(String[] args) {
      LinkedList<Integer> A = new LinkedList<>();
      LinkedList<Integer> B = new LinkedList<>();
      A.add(1);
      A.add(2);
      A.add(3);
      B.add(3);
      B.add(5);
      B.add(6);
      LinkedList<Integer>[] lst = new LinkedList[2];
      lst[0] = A;
      lst[1] = B;
      System.out.println(mergeAll(lst));
      }
  }

