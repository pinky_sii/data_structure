public class DoubleTrouble {

    public static int findRank(int[] arr,int lo,int hi,int k) {
        // less than count +1
        int rank = 0;
        int mid = lo+(hi-lo)/2;
        if (hi >= lo) {
            if (arr[mid] == k)
                return rank += mid;
            else if (k < arr[mid])
                return findRank(arr,lo,mid-1,k);
            else
                return findRank(arr,mid+1,hi,k);
        }
        return mid;
    }

  public static int rank(int[] A, int[] B, int e) {
    // todo: implement me
      int a = findRank(A,0,A.length-1,e);
      int b = findRank(B,0,B.length-1,e);
    return a+b;
  }

  public static int selectHelp(int[] arr, int[] arr2, int lo,int hi,int k) {
      int mid = lo+(hi-lo)/2;
      int r = rank(arr,arr2,arr[mid]);
      if (r == k)
          return arr[mid];
      if (lo == hi) return -1;
      else {
          if (k < r)
              return selectHelp(arr,arr2,lo,mid-1,k);
          else
              return selectHelp(arr,arr2,mid+1,hi,k);
      }
  }

  public static Integer select(int[] A, int[] B, int k) {
    // todo: implement me
      int a = selectHelp(A,B,0,A.length-1,k);
      int b = selectHelp(B,A,0,B.length-1,k);
      if(a!=-1) return a;
      else if (b!=-1) return b;
      else return null;
  }
    public static void main(String[] args) {
        int[] a = {1,4,6,7};
        int[] b = {3,5,8,10};
        System.out.printf("a+b %d ",rank(a,b,66));
        System.out.println("select" + select(a,b,6));

    }
}
