import java.util.*;

public class ExprMonger {
  public static double opr(String op, Double u, Double v) {
    if (op.equals("+")) return u+v;
    else if (op.equals("-")) return u-v;
    else if (op.equals("*")) return u*v;
    else if (op.equals("/")) return u/v;
    else if (op.equals("**")) return Math.pow(u,v);
    else return Double.NaN;
  }

  public static double evalFullyParenthesized(List<String> tokens) {
    Stack<String> ops = new Stack<>();
    Stack<Double> val = new Stack<>();
    for (String token: tokens) {
      if (token.equals("(")) continue;
      else if (token.equals("+")) ops.push(token);
      else if (token.equals("-")) ops.push(token);
      else if (token.equals("*")) ops.push(token);
      else if (token.equals("/")) ops.push(token);
      else if (token.equals("**")) ops.push(token);
      else if (token.equals(")")) {
        String op = ops.pop();
        Double u = val.pop();
        Double v = val.pop();
        val.push(opr(op,v,u));
      }
      else val.push(Double.parseDouble(token));
    }
    return val.pop();
  }

  public static Integer prece(String op) {
      Map<String,Integer> map =new HashMap<>();
      map.put("+",0);
      map.put("-",0);
      map.put("*",1);
      map.put("/",1);
      map.put("**",2);
      if(map.containsKey(op)) {
          return map.get(op);
      } else return -1;
  }

  public static List<String> augmentExpr(List<String> tokens) {
      Stack<String> ops = new Stack<>();
      Stack<String> val = new Stack<>();
      for (String token : tokens) {
          if (token.equals("(")) { ops.add(token);
          continue;
          }
          if (ops.size() == 0 || val.size() ==0) {
              if (prece(token) > -1) ops.add(token);
              else val.add(token);
          }

          else if (prece(token)!= -1) {
              if (prece(token).compareTo(prece(ops.peek())) <= 0) {
                  String u = val.pop();
                  String op = ops.pop();
                  String v = val.pop();
                  String aug = "(" + v + op + u +")" ;
                  val.add(aug);
                  ops.add(token);
              } else {
                  ops.add(token);
              }
          }
          else if (token.equals(")")) {
              String par = "";
              while (!ops.peek().equals("(")) {
                  String u = val.pop();
                  String op = ops.pop();
                  String v = val.pop();
                  String aug = "(" + v + op + u +")" ;
                  val.add(aug);
              }
              ops.pop();
              continue;
          }
          else val.add(token);
      }
      while (val.size()>1 && !ops.isEmpty()) {
          String u = val.pop();
          String op = ops.pop();
          String v = val.pop();
          String aug = "(" + v + op + u +")";
          val.add(aug);
      }

    return Utils.tokenize(val.pop());
  }

  public static double evalExpr(List<String> tokens) {
    return evalFullyParenthesized(augmentExpr(tokens));
  }

    public static void main(String[] args) {
       // System.out.println(evalFullyParenthesized(Utils.tokenize("(1+3)")));
        System.out.println(augmentExpr(Utils.tokenize("1+3/(5-2)*1.5")));
    }
}
