/**
 * Created by Siripatsornsirichai on 5/29/2017 AD.
 */
public class Last {

    public static Integer binarySearchHelper(int[] a, int lo, int hi, int k) {
        if (hi >= lo) {
            int mid = lo+(hi-lo)/2;
            if ((mid == hi || k < a[mid+1]) && a[mid] == k)
                return mid;
            else if (k < a[mid])
                return binarySearchHelper(a,lo,(mid-1),k);
            else
                return binarySearchHelper(a,(mid+1),hi,k);
            }
        return null;
    }

    public static Integer binarySearchLast(int[] a, int k) {
        return binarySearchHelper(a,0,a.length-1,k);
    }
}
