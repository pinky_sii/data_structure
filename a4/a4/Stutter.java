/**
 * Created by Siripatsornsirichai on 5/30/2017 AD.
 */
public class Stutter {
    public static boolean isSubstr(String a, String b) {
        int index = 0;
        for(int i=0; i < b.length(); i++) {
            if(b.charAt(i) == a.charAt(index))
                index++;
            if (index == a.length()) {
                return true;
            }
        }
        return false;
    }

    public static String stutter(String A, int k) {
        if (k == 0) return " ";
        else if (k==1) return A;
        else {
            StringBuilder str = new StringBuilder();
                for (int i = 0; i < A.length(); i++) {
                    for(int j=0; j < k; j++)
                    str.append(A.charAt(i));
                }
                return str.toString();
            }
        }

    public static int maxHelper(String a, String b, int lo, int hi) {
        if (lo == hi) return 0;
        int k = (hi+lo)/2;
        String str = stutter(a,k);
        if (isSubstr(str,b)) {
            int p = maxHelper(a,b,k+1,hi);
            if (p ==0 ) {
                return k;
            } else {
                return maxHelper(a,b,k+1,hi);
            }
        } else {
            return maxHelper(a,b,lo,k);
        }
    }

    public static int maxStutter(String a, String b) {
        int n = a.length();
        int m = b.length();
        int k = m/n+1;
        return maxHelper(a, b,0,k);
    }
}
