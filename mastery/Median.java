import java.util.*;

public class Median {
    public static int quickMedian(TreeMap<Integer, Integer> hist) {
        int value = Integer.MIN_VALUE;
        int k = 0;
        for (Integer key : hist.keySet()) {
            if (hist.get(key) > value) {
                value = hist.get(key);
                k = key;
            }
        }
        return k;
    }
    public static void main(String[] args) {
        TreeMap<Integer, Integer> input0 = new TreeMap<>();
        input0.put(1, 5);
        input0.put(2, 200);
        input0.put(3, 7);
        int expcted0 = 2;

        TreeMap<Integer, Integer> input1 = new TreeMap<>();
        input1.put(1, 2);
        input1.put(2, 5);
        input1.put(3, 1);
        input1.put(4, 9999);
        int expcted1 = 4;

        System.out.println("verdict: "+ (quickMedian(input0)==expcted0));
        System.out.println("verdict: "+ (quickMedian(input1)==expcted1));
    }
}
