import java.util.*;

import com.sun.javafx.geom.Edge;
import graph.WeightedEdge;

import javax.xml.soap.Node;

import static graph.WeightedEdge.Edge;


public class FFA {

    public static int farthestDist(int n, List<WeightedEdge> flights) {
        Map<Integer,Integer> tree = new HashMap<>();
        int[] cost = new int[n+1];
        Arrays.fill(cost,0);
        for (WeightedEdge fl: flights) {
            if(fl.first == 1 && fl.second == 0 ) {
                return 0;
            } else {
                int u = fl.first;
                int v = fl.second;
                int c = fl.cost;
                cost[v] = cost[u] + c;
            }

        }
        Arrays.sort(cost);
        System.out.println(Arrays.toString(cost));
        return cost[cost.length-1];
    }

    public static void main(String[] args) {
        List<WeightedEdge> flights = new ArrayList<>(Arrays.asList(
                Edge(1, 2, 1),
                Edge(2, 3, 2),
                Edge(2, 4, 5)));
        int n = 4;
        int expcted = 6;
        boolean verdict = farthestDist(n, flights)==expcted;
        System.out.println("verdict: "+verdict);
    }

}
