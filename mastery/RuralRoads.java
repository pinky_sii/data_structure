import java.util.*;
import graph.WeightedEdge;
import static graph.WeightedEdge.Edge;

public class RuralRoads {

    public static class Union {
        private int[] id;
        private int[] sz;

        public Union(int n) {
            id = new int[n];
            sz = new int[n];
            for (int i =0; i < n;i++) {
                id[i] = i;
                sz[i] = 1;
            }
        }

        public int root(int[] id, int p) {
            int pid = id[p];
            while (pid!=id[pid]) {
                pid = id[pid];
            }
            return pid;
        }

        public void union(int u, int v) {
            int ru = root(id,u);
            int rv = root(id,v);
            if(sz[ru] > rv) {
                id[rv] = ru;
                sz[ru] += rv;
            } else {
                id[ru] = rv;
                sz[rv] += ru;
            }
        }
    }

    public static int totalWeight(List<WeightedEdge> edges) {
        PriorityQueue<int[]> roads = new PriorityQueue<>((a,b)->a[2]-b[2]);
        Map<Integer,Integer> map = new HashMap<>();
        for (WeightedEdge edge: edges) {
            int u = edge.first;
            int v = edge.second;
            int c = edge.cost;
            int[] e = {u,v,c};
            roads.add(e);
            if (!map.containsKey(u)) {
                map.put(u,v);
            }
            if (!map.containsKey(v)) {
                map.put(v,u);
            }
        }
        //System.out.println(map);

        List<Integer> mst = new ArrayList<>();
        int MST = 0;
        int N = map.size();
        Union un = new Union(N);
        while (mst.size() < N-1) {
            int[] road = roads.poll();
            int x = un.root(un.id, road[0]);
            int y = un.root(un.id, road[1]);
            int cost = road[2];

            if (x!=y) {
                MST+=cost;
                mst.add(cost);
                un.union(x,y);
            }
        }
        return MST;
    }
    public static void main(String[] args) {
       List<WeightedEdge> edges = new ArrayList<>(Arrays.asList(
            Edge(0, 1, 7),
            Edge(0, 3, 5),
            Edge(1, 3, 9),
            Edge(1, 2, 8),
            Edge(1, 4, 7),
            Edge(2, 4, 5),
            Edge(3, 4, 15),
            Edge(3, 5, 6),
            Edge(4, 5, 8),
            Edge(4, 6, 9),
            Edge(5, 6, 11)));
       int expected = 39;
       boolean verdict = totalWeight(edges)==expected;
        System.out.println("verdict: "+verdict);
    }
}
