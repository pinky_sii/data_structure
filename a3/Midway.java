import java.util.Arrays;

/**
 * Created by Siripatsornsirichai on 5/18/2017 AD.
 */
public class Midway {
    public static long stepsRemaining(int[] diskPos) {
        return (1<<diskPos.length)-1- hanoi(diskPos,0,1,2);
    }


    public static long hanoi(int[] arr,int from,int to,int aux) {
        int last = arr.length-1;
        if (arr.length==1) {
            if(arr[last]==from) {return 0;}
            else {return 1;}
        } else {
            if(arr[last] == from) {
                return hanoi(Arrays.copyOfRange(arr,0, arr.length-1),from,aux,to);}
            else {
                return (1<<arr.length-1) + hanoi(Arrays.copyOfRange(arr,0,arr.length-1),aux,to,from);}
        }
    }

}
